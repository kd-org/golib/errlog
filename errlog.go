package errlog

import (
	"context"
	"errors"
	"fmt"
	"log"
)

var ErrLogged = errors.New("")

type OutputType interface {
	Output(calldepth int, s string) error
}

type Logger interface {
	OutputType
	Printf(format string, v ...any)
	Println(v ...any)
}

type Tracer interface {
	Logger
	NoteN(calldepth int, err error)
	Note(err error)
	TraceN(calldepth int, err error) error
	Trace(err error) error
}

var _ Logger = &log.Logger{}

type ErrorLogger struct {
	Logger
	Options
}

type Options struct {
	ShowContextCanceled bool
}

func New(logger Logger, opt *Options) *ErrorLogger {
	if opt == nil {
		opt = &Options{}
	}
	return &ErrorLogger{logger, *opt}
}

func (t ErrorLogger) IsShowError(err error) bool {
	if err == nil {
		return false
	}
	if errors.Is(err, ErrLogged) {
		return false
	}
	if !t.Options.ShowContextCanceled && errors.Is(err, context.Canceled) {
		return false
	}
	return true
}

func (t ErrorLogger) Ignore(err error) {}

func (t ErrorLogger) NoteN(calldepth int, err error) {
	if t.IsShowError(err) {
		t.Output(calldepth+2, err.Error())
	}
}

func (t ErrorLogger) Note(err error) {
	t.NoteN(1, err)
}

func (t ErrorLogger) TraceN(calldepth int, err error) error {
	if t.IsShowError(err) {
		t.Output(calldepth+2, err.Error())
		err = Join(ErrLogged, err)
	}
	return err
}

func (t ErrorLogger) Trace(err error) error {
	return t.TraceN(1, err)
}

type OutputLogger struct {
	OutputType
}

func NewOutput(output OutputType) *OutputLogger {
	return &OutputLogger{output}
}

func (t OutputLogger) Printf(format string, v ...any) {
	t.Output(2, fmt.Sprintf(format, v...))
}

func (t OutputLogger) Println(v ...any) {
	t.Output(2, fmt.Sprintln(v...))
}
