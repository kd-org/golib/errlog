package errlog

import (
	"errors"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestErrorLogger(t *testing.T) {
	t.Parallel()

	buffer := &strings.Builder{}
	logger := New(log.New(buffer, "", log.LstdFlags|log.Llongfile), nil)

	logger.Println("test")
	logger.Note(logger.Trace(os.ErrPermission))

	require.Contains(t, buffer.String(), "errlog_test.go:19: test")
	require.Contains(t, buffer.String(), "errlog_test.go:20: permission denied")
	require.EqualValues(t, 2, strings.Count(buffer.String(), "\n"))
	require.True(t, errors.Is(logger.Trace(os.ErrPermission), os.ErrPermission))
	require.True(t, errors.Is(logger.Trace(os.ErrPermission), ErrLogged))
	require.EqualValues(t, 0, strings.Count(logger.Trace(os.ErrPermission).Error(), "\n"))

	require.NotNil(t, New(NewOutput(log.Default()), nil))
}
